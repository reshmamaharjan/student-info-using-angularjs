var app=angular.module('app',[]);
app.controller('displayController',['$scope','$window',function($scope,$window){
	$scope.stdsarray={
			students:[{
					id : 1,
					name: "Alina",
					address: "Jorpati",
					class: 10,
					mobile: "9841879456"
				},
				{
					id : 2,
					name: "Ashma",
					address: "Banasthali",
					class: 9,
					mobile: "9841879856"
				},
				{
					id : 3,
					name: "Meera",
					address: "Balaju",
					class: 10,
					mobile: "9841879411"
				},
				{
					id : 4,
					name: "Usang",
					address: "Nepaltar",
					class: 10,
					mobile: "9841870056"
				},
				{
					id : 5,
					name: "Sharmila",
					address: "Naranthan",
					class: 9,
					mobile: "9841879555"
				}
				
				],
			selected:{}
		};

	
	//console.log($scope.stdsarray);

	$scope.orderByMe = function(x) {
    	$scope.myOrderBy = x;
  	}


  	$scope.removeStudent=function(x){
  		for( var i = 0; i < $scope.stdsarray.students.length; i++){

  			if($scope.stdsarray.students[i].id==x){

  				var name=$scope.stdsarray.students[i].name;
		  		var isConfirm = confirm("Are you sure to delete the record of "+name +"?");
		  		if(isConfirm){
		  			$scope.stdsarray.students.splice(i,1);
		  			alert("Record of "+name +" has been deleted!!!");
		  			break;
		  		}
  			}
  		}
  	}

  	var defineId = function(){
  		var max=0;
  		for( var i = 0; i<$scope.stdsarray.students.length; i++){
  			if(max<$scope.stdsarray.students[i].id){
  				max=$scope.stdsarray.students[i].id;
  				
  			}

  		}
  		return max;

  	}

  	$scope.addStudent = function(){
  		var student={
  			//id : $scope.id,
  			//id : $scope.stdsarray.students.length+1,
  			id : defineId()+1,
  			address : $scope.add,
  			name : $scope.aname,
  			class : $scope.aclass,
  			mobile : $scope.amobile

  		};
  		$scope.stdsarray.students.push(student);
  	}

  	   $scope.getTemplate = function (student) {
        if (student.id === $scope.stdsarray.selected.id)
         return 'edit';
        else return 'display';
    };
     $scope.editStudent = function (student) {
        $scope.stdsarray.selected = angular.copy(student);
    };

    $scope.saveStudent = function (index) {
        console.log("Saved student details.");
        $scope.stdsarray.students[index] = angular.copy($scope.stdsarray.selected);
        $scope.reset();
    };

    $scope.reset = function () {
        $scope.stdsarray.selected = {};
    };

}]);

